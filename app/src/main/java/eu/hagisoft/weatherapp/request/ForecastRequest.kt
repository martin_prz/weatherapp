package eu.hagisoft.weatherapp.request

import com.google.gson.Gson
import eu.hagisoft.weatherapp.data.Data

class ForecastRequest(val city: String) : Request<Data.CityForecast>() {
    companion object {

        val URL = "http://api.openweathermap.org/data/2.5/forecast?q="
    }
    override fun getUrl(): String {
        return "$URL$city"
    }

    override fun parse(requestResult : String): Data.CityForecast {
        return Gson().fromJson(requestResult, Data.CityForecast::class.java)
    }
}