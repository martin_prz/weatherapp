package eu.hagisoft.weatherapp.request

import android.util.Log
import java.net.URL

abstract class Request<T> {

    companion object {
        val APP_ID = "ed61e31578446ce3902808d6e9ee31d7"
    }

    fun execute(): T? {
        Log.d(javaClass.simpleName, "Request launched...")

        val fullUrl = "${getUrl()}&APPID=$APP_ID"
        val requestResult = URL(fullUrl).readText();

        Log.d(javaClass.simpleName, "Response received: $requestResult")
        var parsedData: T? = null

        try {
            parsedData = parse(requestResult)
        } catch(e: Exception) {
            Log.e(javaClass.simpleName, "Error parsing the response -- ${e.message}")
        }

        Log.d(javaClass.simpleName, "Response parsed.")
        return parsedData
    }

    abstract fun getUrl(): String

    abstract fun parse(requestResult : String): T
}