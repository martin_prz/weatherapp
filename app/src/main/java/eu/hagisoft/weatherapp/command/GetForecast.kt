package eu.hagisoft.weatherapp.command

import eu.hagisoft.weatherapp.model.ForecastModel
import eu.hagisoft.weatherapp.request.ForecastRequest

class GetForecast(val city: String) : Command<ForecastModel> {

    override fun execute(): ForecastModel {
        val response = ForecastRequest(city).execute()
        return if (response == null) ForecastModel() else ForecastModel(response)
    }
}