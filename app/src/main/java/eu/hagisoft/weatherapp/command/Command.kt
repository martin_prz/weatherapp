package eu.hagisoft.weatherapp.command

interface Command<T> {

    fun execute() : T
}