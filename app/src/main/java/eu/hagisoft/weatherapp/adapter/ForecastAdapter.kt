package eu.hagisoft.weatherapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import eu.hagisoft.weatherapp.R
import eu.hagisoft.weatherapp.data.Data
import org.jetbrains.anko.find

class ForecastAdapter(val items: List<Data.Weather>) : RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ViewHolder = ViewHolder(TextView(parent.context))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val weatherIV: ImageView
        private val descriptionTV: TextView
        private val temperatureTV: TextView
        private val pressureTV: TextView

        init {
            weatherIV = view.find(R.id.widget_id_weather_image)
            descriptionTV = view.find(R.id.widget_id_weather_description)
            temperatureTV = view.find(R.id.widget_id_weather_temperature)
            pressureTV = view.find(R.id.widget_id_weather_pressure)
        }

        fun bind(weather: Data.Weather) {
            with(weather) {
                descriptionTV.text = weather.main.temp.toString()
            }
        }
    }
}