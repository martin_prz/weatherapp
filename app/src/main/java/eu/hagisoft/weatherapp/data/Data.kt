package eu.hagisoft.weatherapp.data

class Data {

    data class Coordinates(val lon: Float = 0f,
                           val lat: Float = 0f)

    data class Temperature(val temp: Float = 0f,
                           val temp_min: Float = 0f,
                           val temp_max: Float = 0f,
                           val pressure: Float = 0f,
                           val sea_level: Float = 0f,
                           val ground_level: Float = 0f,
                           val humidity: Int = 0)

    data class Wind(val speed: Float = 0f,
                    val deg: Float = 0f)

    data class Clouds(val all: Int = 0)

    data class Extras(val main: String = "",
                      val description: String = "",
                      val icon: String = "")

    data class Weather(val dt: Long = 0,
                       val main: Temperature = Temperature(),
                       val weather: List<Extras> = listOf(Extras()),
                       val clouds: Clouds = Clouds(),
                       val wind: Wind = Wind(),
                       val dt_txt: String = "")

    data class City(val id : String = "",
                    val name : String = "",
                    val coord : Coordinates = Coordinates(),
                    val country : String = "",
                    val list : List<Weather> = listOf(Weather()))

    data class CityForecast(val city: City = City())
}