package eu.hagisoft.weatherapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import eu.hagisoft.weatherapp.adapter.ForecastAdapter
import eu.hagisoft.weatherapp.command.GetForecast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.async
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val forecast_list: RecyclerView = find(R.id.forecast_list)
        forecast_list.layoutManager = LinearLayoutManager(this)

        button_action.setOnClickListener {
            async() {
                val forecast = GetForecast("London").execute()
                uiThread {
                    toast("request completed")
                    forecast_list.adapter = ForecastAdapter(forecast.getDailyWeatherList())
                }

            }
        }
    }

}
