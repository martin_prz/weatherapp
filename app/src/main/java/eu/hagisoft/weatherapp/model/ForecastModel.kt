package eu.hagisoft.weatherapp.model

import eu.hagisoft.weatherapp.data.Data

class ForecastModel(val forecastData : Data.CityForecast = Data.CityForecast()) {

    fun getDailyWeatherList(): List<Data.Weather> {
        return forecastData.city.list
    }
}